import { Component, TemplateRef, ViewChild, OnInit, ElementRef } from '@angular/core';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { GridOptions, ColDef, RowDoubleClickedEvent, GridApi } from 'ag-grid-community';
import { AngularFireDatabase } from '@angular/fire/database';
import { NbDialogService } from '@nebular/theme';
import { DemandaDashboardInterface } from '../../models/demandaDashboard.model';
import { GetDataService } from 'src/service/getData.service';
import { FormatDashService } from 'src/service/formatDash.service';
import { RemoteControlService } from 'src/service/remoteControl.service';
import { SelectItemInterface } from 'src/models/SelectItem.model';
import { CentralRxJsService } from 'src/service/centralRxjs.service';
import { config } from 'src/service/config';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  esteiras: SelectItemInterface[];
  esteirasSelecionadas: any[];
  DataList: DemandaDashboardInterface[] = [];
  @ViewChild('ModalShowFiltro', { static: true }) Modal_Filtro: TemplateRef<any>;
  @ViewChild('ModalDetail', { static: true }) Modal_Detail: TemplateRef<any>;
  public gridApi: GridApi;
  public gridOptions: GridOptions;
  columnDefs: ColDef[] = [];
  isGridReady = false;
  demanda_onFocus: DemandaDashboardInterface = null;
  constructor(
    public db: AngularFireDatabase,
    public dialogService: NbDialogService,
    public dataSrv: GetDataService,
    public formatSrv: FormatDashService,
    public remoteControl: RemoteControlService,
    public centralRx: CentralRxJsService
  ) {
    moment.locale('pt');
    this.centralRx.DataSended.subscribe((res) => {
      if (res === config.rxjsCentralKeys.ShowFilterEsteiraDashBoard) {
        this.showModalFiltro();
      }
    });
  }

  ngOnInit() {
    this.centralRx.DataSended.subscribe((res) => {
      switch (res) {
        case config.rxjsCentralKeys.ShowFilterEsteiraDashBoard:
          this.showModalFiltro();
          break;
        case config.rxjsCentralKeys.ChangeToWeb:
          console.log('Mudando para Forma Web');
          window.onresize = ((resizeObj) => {
            this.gridApi.sizeColumnsToFit();
          });
          this.startGrid();
          this.dataSrv.ListarItems.subscribe((resItems: DemandaDashboardInterface[]) => {
            this.DataList = resItems;
            this.dataSrv.DataJson = resItems;
            this.dataSrv.DataJSalva = resItems;
            this.esteiras = this.dataSrv.listaEsteiras();
            this.gridOptions.api.setRowData(this.DataList);
            this.gridOptions.api.sizeColumnsToFit();
          });
          break;
        case config.rxjsCentralKeys.ChangeToMobile:
          console.log('Mudando para Forma Mobile');
          break;
      }
    });
  }
  startGrid() {
    this.gridOptions = {
      columnDefs: this.createColumnDefs,
      enableSorting: true,
      onRowDoubleClicked: ((event) => {
        this.onRowSelect(event);
      }),
      headerHeight: 0,
      rowHeight: 100,
      getRowStyle: (params) => {
        return this.formatSrv.formatarGridColor(params.data);
      },
      onGridReady: (params) => {
        this.gridApi = params.api;
        this.gridApi.sizeColumnsToFit();
        console.log('grid de DashBoard Pronta!');
        const comando = config.rxjsCentralKeys.GridReady;
        this.centralRx.sendData = comando;
        this.isGridReady = true;
      }
    };


  }


  onRowSelect(rowData: RowDoubleClickedEvent) {
    console.log(rowData.data);
    this.demanda_onFocus = rowData.data;
    this.dialogService.open(this.Modal_Detail);
  }

  get createColumnDefs(): Array<any> {
    const that = this;
    return this.columnDefs = [
      {
        headerName: 'Esteira',
        field: 'esteira',
        width: 80,
        // height: 190,
        cellRenderer: that.formatSrv.MontarColunaEsteira,
      },
      {
        headerName: 'Restante',
        field: 'data',
        width: 20,
        autoHeight: true,
        cellRenderer: this.formatSrv.MontarColunaRestante,
      },
      {
        headerName: 'Status',
        field: 'status',
        width: 30,
        autoHeight: true,
        cellRenderer: this.formatSrv.MontarColunaStatus,
      },
    ];
  }

  filtrarLista(esteira, refs) {
    this.gridApi.setRowData(this.dataSrv.filtroEsteira(esteira));
    this.gridOptions.api.sizeColumnsToFit();
    refs.close();
  }
  showModalFiltro() {
    this.remoteControl.DashBoardAtivo();
    this.dialogService.open(this.Modal_Filtro);
  }


}
