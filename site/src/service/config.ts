export const config = {
  rxjsCentralKeys: {
    GridResize: 'gridResize',
    GridReady: 'gridReady',
    ShowFilterEsteiraDashBoard: 'showFilterEsteiraDashBoard',
    ShowFilterEsteiraGD: 'ShowFilterEsteiraGD',
    ChangeToMobile: 'changeToMobile',
    ChangeToWeb: 'changeToWeb',
    onRegisterUserSucess: 'onRegisterUserSucess',
    onLoginUserSucess: 'onLoginUserSucess',
    pwaUpdateRequest: 'pwaUpdateRequest'
  },
  localStorageKeys: {
    userRefreshToken: 'userRefreshToken',
    tokenUserOnPageLoad: 'tokenUserOnPageLoad'
  },
  animateCssArr: ['bounceIn', 'bounceInDown',
    'bounceInLeft', 'bounceInRight', 'bounceInUp', 'bounceOut',
    'bounceOutDown', 'bounceOutLeft', 'bounceOutRight', 'bounceOutUp',
    'fadeIn', 'fadeInDown', 'fadeInDownBig,	fadeInLeft',
    'fadeInLeftBig', 'fadeInRight', 'fadeInRightBig', 'fadeInUp',
    'fadeInUpBig', 'fadeOut', 'fadeOutDown', 'fadeOutDownBig',
    'fadeOutLeft', 'fadeOutLeftBig', 'fadeOutRight', 'fadeOutRightBig',
    'fadeOutUp', 'fadeOutUpBig', 'flipInX', 'flipInY',
    'flipOutX', 'flipOutY', 'lightSpeedIn', 'lightSpeedOut',
    'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft',
    'rotateInUpRight', 'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight',
    'rotateOutUpLeft', 'rotateOutUpRight',
    'rollIn', 'rollOut', 'zoomIn', 'zoomInDown',
    'zoomInLeft', 'zoomInRight', 'zoomInUp', 'zoomOut',
    'zoomOutDown', 'zoomOutLeft', 'zoomOutRight', 'zoomOutUp',
    'slideInDown', 'slideInLeft', 'slideInRight', 'slideInUp',
    'slideOutDown', 'slideOutLeft', 'slideOutRight', 'slideOutUp'],
  fabCommand: {
    FiltrarGrid: 'FiltrarGrid',
    GoToDash: 'GoToDash',
    GoToGD: 'GoToGD',
    GoToUser: 'GoToUser',
  },
  errsLabelForToast: {
input: {
  nome: {
    title: 'O seu nome não foi informado',
    subtitle: 'O campo é obrigatório'
  },
  emailVazio: {
    title: 'Email não informado',
    subtitle: 'O campo é obrigatório'
  },
  emailInvalido: {
    title: 'Email informado é invalido',
    subtitle: 'inclua um email valido no campo'
  }
}
  }
}
