import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import * as firebase from 'firebase';
import { AngularFireDatabase } from '@angular/fire/database';
import { UserObjInterface } from 'src/models/userObj.model';
import { LoginService } from './login.service';
import { config } from './config';
import { CentralRxJsService } from './centralRxjs.service';
import { ToastService } from './toast.service';
import { SwUpdate } from '@angular/service-worker';
import * as lodash from 'lodash';
@Injectable()
export class NotificationService {
  //messaging = firebase.messaging();
  Envio = '';
  sessionToken;
  constructor(
    private afMessaging: AngularFireMessaging,
    private db: AngularFireDatabase,
    private loginSrv: LoginService,
    private centralRxjs: CentralRxJsService,
    private swUpdate: SwUpdate,
    private toastSrv: ToastService) {
    this.checkForUpdate();
    this.swUpdate.available.subscribe(event => {
      const comando = config.rxjsCentralKeys.pwaUpdateRequest;
      this.centralRxjs.sendData = comando;
    });

    this.afMessaging.messages
      .subscribe((message) => {
        console.log('linha 10 do serviço ', message);
      });

    this.afMessaging.requestPermission.subscribe((res) => {
      console.log('resultado do pedido: ', res);
    });

    this.afMessaging.requestToken
      .subscribe(
        (token) => {
          console.log('Permission granted! Save to the server!', token);
          this.sessionToken = token;
          sessionStorage.setItem(config.localStorageKeys.tokenUserOnPageLoad, token);
          this.db.list('/tokenForPush').push(this.sessionToken).then(() => {
            this.toastSrv.sendSucessToast({
              title: 'Token Adiconado',
              subtitle: 'Você agora ira receber atualizações sobre as demandas'
            });
          });
          this.updateTokens();
        },
        (error) => {
          this.sessionToken = sessionStorage.getItem(config.localStorageKeys.tokenUserOnPageLoad);
          console.error(error);
        },
      );
  }
  checkForUpdate() {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.checkForUpdate().then(() => {
        console.log('Checking for updates...');
      }).catch((err) => {
        console.error('Error when checking for update', err);
      });
    }
  }


  updateTokens() {
    this.db.list('/tokenForPush').valueChanges()
      .subscribe((tokenArr) => {
        tokenArr.push(this.sessionToken);
        console.clear();
        console.log(lodash.uniq(tokenArr));
        this.db.object('/tokenForPush').set(lodash.uniq(tokenArr)).then(() => {
          console.log('funcinou!');

        });
      });
  }

}

