import { DemandaDashboardInterface } from '../models/demandaDashboard.model';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { map, filter, uniq, orderBy } from 'lodash';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import { config } from './config';
import { FabListInterface } from 'src/models/fabList.model';

@Injectable()
export class GetDataService {

  private _DataJson: DemandaDashboardInterface[] = [];
  private _DataJSalva: DemandaDashboardInterface[] = [];
  private _localRequisitado = environment.NomeDashBoard;
  public ControleRemoto$ = new Subject<any>();
  constructor(public db: AngularFireDatabase) { }

  get ListarItems() {
    return this.db.list('brq-sla/ONS').valueChanges();
  }

  get ListarGerenciamento() {
    return this.db.list('brq-sla/gerenciamentoDiario').valueChanges();
  }

  filtroEsteira(esteiras) {
    const result = [];
    esteiras.forEach(esteira => {
      filter(this.DataJSalva, {'esteira': esteira.name }).forEach(x => {
        result.push(x);
      });
    });
    console.log(result);
    this.DataJson = orderBy(result, ['data', 'esteira']);
    // filter(this.DataJson,{'esteira': esteira});
    return this.DataJson;
  }
  listaEsteiras() {
    return map(uniq(map(this.DataJson, 'esteira')), (item, index) => {
      return { label: item, value: { id: index, name: item } };
    });
  }

  public set DataJson(value: DemandaDashboardInterface[]) {

    this._DataJson = value;
  }

  public get DataJson(): DemandaDashboardInterface[] {

    return this._DataJson;
  }

  public set DataJSalva(value: DemandaDashboardInterface[]) {

    this._DataJSalva = value;
  }

  public get DataJSalva(): DemandaDashboardInterface[] {

    return this._DataJSalva;
  }

  public get randomEffect() {
    const effArr = config.animateCssArr;
    const random = effArr[Math.floor(Math.random() * effArr.length)];
    return random;
  }


  public get FabButtonList() {
    const arrFab: FabListInterface[] = [
      {
      cor: 'info',
      icon: 'fa fa-search',
      comando: config.fabCommand.FiltrarGrid,
      tooltip: 'Buscar'
    },
    {
      cor: 'warning',
      icon: 'fas fa-clock',
      comando: config.fabCommand.GoToDash,
      tooltip: 'DashBoard'
    },
    {
      cor: 'success',
      icon: 'fas fa-table',
      comando: config.fabCommand.GoToGD,
      tooltip: 'Gerenciamento Diario'
    },
/*     {
      cor: 'danger',
      icon: 'fas fa-user',
      comando: config.fabCommand.GoToUser,
      tooltip: 'Log in'
    } */

    ];
    return arrFab;
  }

  public get splashScreenImage() {

// tslint:disable-next-line: max-line-length
return 'https://firebasestorage.googleapis.com/v0/b/brq-sla.appspot.com/o/logodeSplashScreen%2F2.9.1v.png?alt=media&token=1b5db5a9-a79f-4c91-aa41-b2dd56582ee1';
  }
}
