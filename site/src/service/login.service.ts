import { DemandaDashboardInterface } from '../models/demandaDashboard.model';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { config } from './config';
import { UserObjInterface } from 'src/models/userObj.model';
import { NbToastrService } from '@nebular/theme';
import { CentralRxJsService } from './centralRxjs.service';
import * as moment from 'moment'
@Injectable()
export class LoginService {

  private _UserObj: UserObjInterface;
  Envio: string;
  constructor(
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private toastrService: NbToastrService,
    private centralRxjs: CentralRxJsService) {
    const refreshToken = localStorage.getItem(config.localStorageKeys.userRefreshToken)
    if (refreshToken !== null) {
      this.afAuth.auth.signInWithCustomToken(refreshToken)
        .then((res) => {
          console.log('Login Automatico');
          localStorage.setItem(config.localStorageKeys.userRefreshToken, res.user.refreshToken);
          this.getUserDataFromLocal(res.user.displayName, res.user.uid);
          const comando = config.rxjsCentralKeys.onLoginUserSucess;
          this.centralRxjs.sendData = comando;
        }).catch((err) => {
          console.log(err);

        })
    } else {
      console.log('linha 32 de loginservice');

    }
  }

  logInUser(loginObj: UserObjInterface) {
   return this.afAuth.auth.signInWithEmailAndPassword(loginObj.email, loginObj.password);
  }

  registerNewUser(UserObj: UserObjInterface) {
    console.log('Linha 47 do serviço de Login', UserObj.dataNascimento );

    //const dataEmUnix = UserObj.dataNascimento.format('x');

    UserObj.dataNascimento = Number(UserObj.dataNascimento);
   return this.afAuth.auth.createUserWithEmailAndPassword(UserObj.email, UserObj.password)
  }

  saveNewUserToDB(UserObj: UserObjInterface) {
    this.Envio = 'Usuarios/' + UserObj.nomeCompleto + ' - ' + UserObj.uuid;
    /* this.UserObj.tokenForPush = undefined; */
    this.db.object(this.Envio).set(UserObj)
      .then((res) => {
        this.toastrService.success(
          'Usuario Criado e incluido no banco de dados',
          `Usuario Criado com Sucesso`,
          {
            destroyByClick: true,
            icon: 'fas fa-check',
          });
        const comando = config.rxjsCentralKeys.onRegisterUserSucess;
        this.centralRxjs.sendData = comando;
      }).catch((err) => {
        console.log(err);
      });
  }
  get NewUserObj(): UserObjInterface {
    return {
      email: null as string,
      password: null as any,
      uuid: null as string,
      nomeCompleto: null as string,
      dataNascimento: null as any,
      cargo: null as string,
      isAdm: false as boolean,
      exteira: null as string,
      tokenForPush: []
    };
  }

  async getUserDataFromLocal(displayName: string, uuid: string) {
    console.log('Entrei na linha 86');
    this.Envio = 'Usuarios/' + displayName + ' - ' + uuid;
    return this.db.object(this.Envio).valueChanges().toPromise();
/*     const dbUser: any = await this.db.object(this.Envio).valueChanges().toPromise();
    this.UserObj = dbUser;
    return this.UserObj; */
  }
  get UserObj() {
    return this._UserObj;
  }

  set UserObj(value: UserObjInterface) {
    this._UserObj = value;
  }

}

