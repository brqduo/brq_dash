export interface DemandaDashboardInterface {
  area: string;
  criticidade: string;
  data: string;
  dataFormatada: string;
  datafim: string;
  esteira: string;
  sistema: string;
  status: string;
  tfs: string;
  tiposla: string;
  titulo: string;
}
