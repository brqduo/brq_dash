export interface UserObjInterface {
  email: string;
  password: any;
  uuid: string;
  nomeCompleto: string;
  dataNascimento: any;
  cargo: any;
  isAdm: boolean;
  exteira: string;
  tokenForPush: Array<string>;
}
