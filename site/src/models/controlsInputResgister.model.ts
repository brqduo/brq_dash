import { FormControl } from '@angular/forms';

export interface ControlsInputRegisterInterface {
    dataNascimento: FormControl;
    email: FormControl;
    exteira: FormControl;
    nomeCompleto: FormControl;
    senha: FormControl;
}
