import { trigger, state, style, animate, transition } from '@angular/animations';
export const animationsSnackBar = [
  trigger('showSnackBar', [
    state('false', style({
      opacity: 0,
      bottom: '0',
      visibility: 'unset',
    })),
    state('true', style({
      visibility: 'visible',
      opacity: 1,
      bottom: '30px'
    })),
    transition('false=>true', animate('500ms')),
    transition('true=>false', animate('500ms'))
  ]),
]
