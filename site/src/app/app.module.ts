import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
// Localidade
import { registerLocaleData } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import localePT from '@angular/common/locales/pt';

// Nebular

import { NbThemeModule, NbDialogModule } from '@nebular/theme';
import { NbMomentDateModule } from '@nebular/moment';
import { NebularModule } from '../Modules/Nebular.module';
/* import { NbEvaIconsModule } from '@nebular/eva-icons';  */

// Ag-Grid
import { AgGridModule } from 'ag-grid-angular';

// AngularFire
import { environment } from './../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireStorageModule } from '@angular/fire/storage';

// Paginas
import { DashboardComponent } from '../pages/Dashboard/dashboard.component';
import { GDBoardComponent } from '../pages/GD-Board/gd-board.component';
import { RoutingModule } from './app.routing';
import { FormatGDService } from '../service/formatGD.service';

// Componentes
import { LoginModalComponent } from '../components/login-modal/login-modal.component';
import { SnackbarComponent } from 'src/components/snackbar/snackbar.component';

// Service
import { GetDataService } from '../service/getData.service';
import { FormatDashService } from '../service/formatDash.service';
import { RemoteControlService } from '../service/remoteControl.service';
import { CentralRxJsService } from '../service/centralRxjs.service';
import { LoginService } from '../service/login.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ToastService } from 'src/service/toast.service';
import { NotificationService } from 'src/service/notification.service';


registerLocaleData(localePT, 'pt');

const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  AngularFireMessagingModule,
  AngularFireStorageModule
]

const FormModules = [
  ReactiveFormsModule,
  FormsModule
]
const Nebular = [
  NebularModule.forRoot(),
  NbDialogModule.forRoot(),
  NbMomentDateModule
]
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    GDBoardComponent,
    LoginModalComponent,
    SnackbarComponent
  ],
  entryComponents: [
    SnackbarComponent,
    LoginModalComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    HttpClientModule,
    ...FormModules,
    // CDK
    BrowserAnimationsModule,
    LayoutModule,
    // AngularFire
    ...AngularFire,
    // Grid
    AgGridModule.withComponents([]),
    // Nebular
    ...Nebular,
    // PWA
    ServiceWorkerModule.register('combined-sw.js', { enabled: environment.production })
  ],
  providers: [
    GetDataService,
    FormatDashService,
    FormatGDService,
    CentralRxJsService,
    RemoteControlService,
    LoginService,
    ToastService,
    NotificationService,
    { provide: LOCALE_ID, useValue: 'pt' },
/*    Location,
   {provide: LocationStrategy, useClass: PathLocationStrategy} */
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
