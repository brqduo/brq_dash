import { FabListInterface } from 'src/models/fabList.model';
import { config } from './../service/config';
import { Component, TemplateRef, ViewChild, OnInit, ElementRef, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { DemandaDashboardInterface } from '../models/demandaDashboard.model';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

import { GetDataService } from 'src/service/getData.service';
import { FormatDashService } from '../service/formatDash.service';
import { RemoteControlService } from '../service/remoteControl.service';
import { CentralRxJsService } from '../service/centralRxjs.service';

import { slideInAnimation } from './route.animation';
import { Router } from '@angular/router';

import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { NbToastrService } from '@nebular/theme';

import { LoginModalComponent } from 'src/components/login-modal/login-modal.component';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { NotificationService } from 'src/service/notification.service';
import { AngularFireDatabase } from '@angular/fire/database';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [slideInAnimation]
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'centralDev';
  DataList: DemandaDashboardInterface[] = [];
  SplashScreen = {
    show: true,
    animation: 'jello slow delay-1s',
    image: null
  };
  showFab = false;
  ShowWhenSizable: boolean;
  showSnackBar = false;
  @ViewChild('animateSplash', { static: false }) animationDiv: ElementRef<any>;
  @ViewChild('dialog', { static: false }) dialog;
  isAppLoaded = false;
  FabList: FabListInterface[] = [];
  constructor(
    public dataSrv: GetDataService,
    public formatSrv: FormatDashService,
    public breakpointObserver: BreakpointObserver,
    public centralRx: CentralRxJsService,
    public route: Router,
    private dialogService: NbDialogService,
    public notificationSrv: NotificationService,
    private afMessaging: AngularFireMessaging,
    private db: AngularFireDatabase
  ) {
    this.centralRx.DataSended.subscribe((res) => {
      switch (res) {
        case config.rxjsCentralKeys.GridReady:
          if (!this.isAppLoaded) {
            this.splashScreenLoadOut();
          }
          break;
        case config.rxjsCentralKeys.onRegisterUserSucess:

          break;
        case config.rxjsCentralKeys.pwaUpdateRequest:
          this.showSnackBar = true;
          break;
      }
    });

    moment.locale('pt');
  }

  ngOnInit() {
    this.SplashScreen.image = this.dataSrv.splashScreenImage;
    this.FabList = this.dataSrv.FabButtonList;
    this.dataSrv.ListarItems.subscribe((res: DemandaDashboardInterface[]) => {
      this.DataList = res;
      this.dataSrv.DataJson = res;
      this.dataSrv.DataJSalva = res;

    });
    this.breakpointObserver
      .observe(['(min-width: 830px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          console.log('Viewport is 500px or over!', state);
          this.centralRx.sendData = config.rxjsCentralKeys.ChangeToWeb;
          this.ShowWhenSizable = false;
        } else {
          console.log('Viewport is getting smaller!', state);
          this.centralRx.sendData = config.rxjsCentralKeys.ChangeToMobile;
          this.ShowWhenSizable = true;
        }
      });
  }
  splashScreenLoadOut() {
    this.animationDiv.nativeElement.addEventListener('animationend', ((res) => {
      if (res.animationName === 'jello') {
        if (this.DataList !== [] || this.DataList !== null || this.DataList !== undefined) {
          this.SplashScreen.animation = 'slideOutUp fast';
        }
      }
      if (res.animationName === 'slideOutUp') {
        this.SplashScreen.show = false;
        this.isAppLoaded = true;
        this.splashScreenLoadOut();
      }
    }));
  }
  openFabMenu() {
    this.showFab = !this.showFab;
  }

  showLogin() {
    this.dialogService.open(LoginModalComponent);
  }
  GoTo(ev) {
    console.log(ev);
    switch (ev.comando) {
      case config.fabCommand.FiltrarGrid:
        this.showFab = false;
        if (this.route.url === '/dashboard') {
          const comando = config.rxjsCentralKeys.ShowFilterEsteiraDashBoard;
          this.centralRx.sendData = comando;
        } else {
          alert('Ainda será implementado');
        }
        break;
      case config.fabCommand.GoToDash:
        this.showFab = false;
        this.route.navigateByUrl('/dashboard');
        break;
      case config.fabCommand.GoToGD:
        this.showFab = false;
        this.route.navigateByUrl('/gdboard');
        break;
      /* case config.fabCommand.GoToUser:
        this.showLogin();
        this.showFab = false;
        break; */
      default:
        break;
    }

  }
  teste() {
    this.showSnackBar = !this.showSnackBar;
 /*    this.afMessaging.requestPermission.subscribe((res) => {
      console.log('resultado do pedido: ', res)
    });
    this.afMessaging.requestToken
      .subscribe(
        (token) => { console.log('Permission granted! Save to the server!', token); },
        (error) => { console.error(error); },
      );
    const Envio = 'DeviceId/teste/arrT';
    this.db.list(Envio).push('omega Max Megazord'); */
  }

  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.

  }

}
