export interface DemandaDashboardModel {
    area: string;
    criticidade: string;
    data: string;
    dataFormatada: string;
    datafim: string;
    esteira: string;
    sistema: string;
    status: string;
    tfs: string;
    tiposla: string;
    titulo: string;
}

export interface PayLoadInterface {
    notification: {
        title: string;
        body: string;
        click_action?: string;
        icon: string;
        color: string;
        badge?: string;
        tap: 'false' | 'true';
        tag?: string;
    }
    data?: any
}

        /*     interface NotificationMessagePayload  {
                tag?: string;
                body?: string;
                icon?: string;
                badge?: string;
                color?: string;
                sound?: string;
                title?: string;
                bodyLocKey?: string;
                bodyLocArgs?: string;
                clickAction?: string;
                titleLocKey?: string;
                titleLocArgs?: string;
                [key: string]: string | undefined;
              };
            
              interface MessagingPayload  {
                data?: admin.messaging.DataMessagePayload;
                notification?: admin.messaging.NotificationMessagePayload;
              };
            
              interface MessagingOptions  {
                dryRun?: boolean;
                priority?: string;
                timeToLive?: number;
                collapseKey?: string;
                mutableContent?: boolean;
                contentAvailable?: boolean;
                restrictedPackageName?: string;
                [key: string]: any | undefined;
              }; */