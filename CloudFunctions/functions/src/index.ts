import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { pushService } from './pushService/push.service'
import { saldoService } from './calculoGD/saldo.service';
import { metaCumpridaService } from './calculoGD/metaCumprida.service';
admin.initializeApp();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript

export const pushNotification = functions.database.ref('/brq-sla/ONS').onUpdate(
    async (snapshot, context) => {
        await pushService(snapshot, context)
    })
export const CalculoMetaCumpridaGD = functions.database.ref('/brq-sla/gerenciamentoDiario').onWrite(
    async (snapshot, context) => {
        await metaCumpridaService(snapshot, context);
    })

export const CalculoSaldoGD = functions.database.ref('/brq-sla/gerenciamentoDiario').onWrite(
    async (snapshot, context) => {
        await saldoService(snapshot, context)
    })