import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { DemandaDashboardModel, PayLoadInterface } from '../models/push.interface';

export const pushService = (async (
    snapshot: functions.Change<functions.database.DataSnapshot>,
    context: functions.EventContext) => {
    try {
        const Antes = snapshot.before.toJSON();
        console.log('Dados de antes', Antes);
        const Depois = snapshot.after.toJSON();
        console.log('Dados depois', Depois);

        const limites = {
            warning: 600,
            danger: 300,
            crazy: 120
        }
        const timeZones = {
            manha: 620,
            tarde: 902,
            final: 1020
        }
        const iconForPush = {
            warning: 'https://firebasestorage.googleapis.com/v0/b/brq-sla.appspot.com/o/iconsForNotification%2FWarning.png?alt=media&token=fca6adf5-45fa-423f-b651-9118fb499f80',
            danger: 'https://firebasestorage.googleapis.com/v0/b/brq-sla.appspot.com/o/iconsForNotification%2FDanger.png?alt=media&token=b1bd2112-5938-4f0f-9c95-b928251a660e',
            crazy: 'https://firebasestorage.googleapis.com/v0/b/brq-sla.appspot.com/o/iconsForNotification%2FCrazy.png?alt=media&token=fbf59ccd-7f3b-48f4-96f6-b065c218a115'
        }
        let arrDemandasObj: DemandaDashboardModel[] = [];
        const ArrayDeDemandas = await admin.database().ref('/brq-sla/ONS').once('value');
        console.log('Aqui esta o array de demandas', ArrayDeDemandas.val());

        arrDemandasObj = ArrayDeDemandas.val();

        let arrTokens: string[] = [];
        const arrTokensFromDB = await admin.database().ref('/tokenForPush').once('value');
        arrTokens = arrTokensFromDB.val();

        /**
         *Transforma hora para minuto
         * 
         * */
        const hourToMinute = (hh: string): number => {
            const arrayHora = hh.split(':');
            console.log('Ativando função de conversão de horas');

            return (Number(arrayHora[0]) * 60) + Number(arrayHora[1]);
        }


        /**
         *Gerar Body 
         * 
         * */
        const gerarBody = ((minutos: number, demandaObj: DemandaDashboardModel) => {
            let retornoText = '';
            if (((minutos >= limites.danger) && (minutos <= limites.warning))) {
                retornoText = 'A demanda "' + demandaObj.tfs + ' - ' + demandaObj.titulo + '" esta com menos de 10 horas de duração'
            }
            if (((minutos >= limites.crazy) && (minutos <= limites.danger))) {
                retornoText = 'A demanda "' + demandaObj.tfs + ' - ' + demandaObj.titulo + '" esta com menos de 5 horas de duração!'
            }
            if ((minutos <= limites.crazy)) {
                retornoText = 'A demanda "' + demandaObj.tfs + ' - ' + demandaObj.titulo + '" esta com menos de 2 horas de duração!'
            }
            return retornoText
        })
        /**
         * Enviar notificação Negra
         *  
         * */
        const CreateCrazyNotification = (demandaObj: DemandaDashboardModel) => {
            const payload: PayLoadInterface = {
                notification: {
                    title: 'Demanda de ' + demandaObj.esteira + '💀💀💀',
                    tap: 'true',
                    body: gerarBody(limites.crazy, demandaObj),
                    icon: iconForPush.crazy,
                    color: '#000000'
                }
            }
            SendPushNotification(arrTokens, payload)
        }
        /**
         *Enviar notificação Vermelha
         * 
         * */
        const CreateDangerNotification = (demandaObj: DemandaDashboardModel) => {
            const payload: PayLoadInterface = {
                notification: {
                    title: 'Demanda de ' + demandaObj.esteira + '🔥🔥🔥',
                    tap: 'true',
                    body: gerarBody(limites.danger, demandaObj),
                    icon: iconForPush.danger,
                    color: '#ff0000'
                }
            }
            SendPushNotification(arrTokens, payload)
        }
        /**
          *Enviar notificação Amarela
          * 
          * */
        const CreateWarningNotification = (demandaObj: DemandaDashboardModel) => {
            const payload: PayLoadInterface = {
                notification: {
                    title: 'Demanda de ' + demandaObj.esteira + '😱😱😱',
                    tap: 'true',
                    body: gerarBody(limites.warning, demandaObj),
                    icon: iconForPush.warning,
                    color: '#ffff00'
                }
            }
            SendPushNotification(arrTokens, payload)
        }
        /**
         *Envia as notificações
         * 
         *   */
        const SendPushNotification = ((token: Array<string>, payload: PayLoadInterface) => {
            const whatTimeIsIt = (((new Date().getHours() * 60) + new Date().getMinutes()) - 180);
            console.log('Q horas ela esta rodando?', whatTimeIsIt);


            switch (whatTimeIsIt) {
                case timeZones.manha:
                    admin.messaging().sendToDevice(token, payload)
                        .then(async (onPushEndDaManha) => {
                            console.log('Enviei as notificações de manhã', onPushEndDaManha);
                            for (let i = 0; i < onPushEndDaManha.results.length; i++) {
                                const element =  onPushEndDaManha.results[i];
                                if (element.error) {
                                  token.splice(i, 1)
                                 // const tokenAfterPush = 
                                  console.log('Lista de tokens após a limpeza', JSON.stringify(token));
                                 await admin.database().ref('/tokenForPush').set(token);
                                }
                              }
                        }).catch((err) => {
                            console.error('Erro no envio', err);
                        })
                    break;
                case timeZones.tarde:
                    admin.messaging().sendToDevice(token, payload)
                        .then(async (onPushEndDaTarde) => {
                            console.log('Enviei as notificações durante a tarde', onPushEndDaTarde);
                            for (let i = 0; i < onPushEndDaTarde.results.length; i++) {
                                const element =  onPushEndDaTarde.results[i];
                                if (element.error) {
                                  token.splice(i, 1)
                                 // const tokenAfterPush = 
                                  console.log('Lista de tokens após a limpeza', JSON.stringify(token));
                                 await admin.database().ref('/tokenForPush').set(token);
                                }
                              }
                        }).catch((err) => {
                            console.error('Erro no envio', err);
                        })
                    break;
                case timeZones.final:
                    admin.messaging().sendToDevice(token, payload)
                        .then(async (onPushEndDoFinal) => {
                            console.log('Enviei as notificações ao final do dia', onPushEndDoFinal);
                            for (let i = 0; i < onPushEndDoFinal.results.length; i++) {
                                const element =  onPushEndDoFinal.results[i];
                                if (element.error) {
                                  token.splice(i, 1)
                                 // const tokenAfterPush = 
                                  console.log('Lista de tokens após a limpeza', JSON.stringify(token));
                                 await admin.database().ref('/tokenForPush').set(token);
                                }
                              }
                        }).catch((err) => {
                            console.error('Erro no envio', err);
                        })
                    break;
            }
        })
        arrDemandasObj.forEach((element: DemandaDashboardModel) => {
            console.log('Dentro do forEach do array de Demandas');
            const minutos = hourToMinute(element.data);
            console.log('Minutos da demanda em questão', minutos);
            if (((minutos >= limites.danger) && (minutos <= limites.warning))) {
                console.log('Ativando CreateWarningNotification');
                CreateWarningNotification(element);
            }
            if (((minutos >= limites.crazy) && (minutos <= limites.danger))) {
                console.log('Ativando CreateDangerNotification');
                CreateDangerNotification(element);
            }
            if ((minutos <= limites.crazy)) {
                console.log('Ativando CreateCrazyNotification');
                CreateCrazyNotification(element);
            }
        });
    }
    catch (error) {
        console.log('O que deu errado aqui?', error);
    }
})