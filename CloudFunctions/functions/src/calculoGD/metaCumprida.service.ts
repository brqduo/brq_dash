import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { GDInterface } from '../models/GDInterface.interface';

export const metaCumpridaService = (async (
    snapshot: functions.Change<functions.database.DataSnapshot>,
    context: functions.EventContext) => {
        try {
            let GDEmGeral: GDInterface[] = [];
            const getGD = (async () => {
                const result = await admin.database().ref('/brq-sla/gerenciamentoDiario').once('value');
                GDEmGeral = result.val();
                GDEmGeral.forEach((element: GDInterface) => {
                    const MetaCumprida = (element.dados.metas.horasEntregues / element.dados.metas.prodMensal) * 100
                    element.dados.metas.metaCumprida = Math.round(MetaCumprida)
                });
                await concluirPush(GDEmGeral);
            })

            const concluirPush = (async (arrModificado: GDInterface[]) => {
                await admin.database().ref('/brq-sla/gerenciamentoDiario').set(arrModificado);
                console.log('Vindo da linha 79', arrModificado);
            })

            await getGD();
        }
        catch (error) {
            console.log('erro da fução CalculoMetaCumprida', error);

        }
    })