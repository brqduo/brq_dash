import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { GDInterface } from '../models/GDInterface.interface';

export const saldoService = (async (
    snapshot: functions.Change<functions.database.DataSnapshot>,
    context: functions.EventContext) => {
        try {
            let GDEmGeral: GDInterface[] = [];
            const getGD = (async () => {
                const result = await admin.database().ref('/brq-sla/gerenciamentoDiario').once('value');
                GDEmGeral = result.val();
                GDEmGeral.forEach((element: GDInterface) => {
                    let Saldo: any = (element.dados.metas.prodMensal - element.dados.metas.horasEntregues)
                    if (Saldo < 0) {
                        console.log('saldo é negativo da esteira ' + element.nome, Saldo);
                        Saldo = '✓'
                        element.dados.saldo = Saldo
                    } else {
                        element.dados.saldo = Saldo
                    }
                });
                await concluirPush(GDEmGeral);
            })

            const concluirPush = (async (arrModificado: GDInterface[]) => {
                await admin.database().ref('/brq-sla/gerenciamentoDiario').set(arrModificado);
                console.log('Vindo da linha 79', arrModificado);
            })

            await getGD();
        }
        catch (error) {
            console.log('erro da fução CalculoMetaCumprida', error);

        }
    })