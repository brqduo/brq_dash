"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var admin = __importStar(require("firebase-admin"));
var serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://brq-sla.firebaseio.com"
});
;
var GDEmGeral = [];
var getGD = (function () { return __awaiter(void 0, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, admin.database().ref('/brq-sla/gerenciamentoDiario').once('value')];
            case 1:
                result = _a.sent();
                GDEmGeral = result.val();
                //   console.log(GDEmGeral);
                GDEmGeral.forEach(function (element) {
                    var MetaCumprida = (element.dados.metas.horasEntregues / element.dados.metas.prodMensal) * 100;
                    element.dados.metas.metaCumprida = Math.round(MetaCumprida);
                    var Saldo = (element.dados.metas.prodMensal - element.dados.metas.horasEntregues);
                    if (Saldo < 0) {
                        console.log('saldo é negativo da esteira ' + element.nome, Saldo);
                        Saldo = ' ✓ ';
                        element.dados.saldo = Saldo;
                    }
                    else {
                        element.dados.saldo = Saldo;
                    }
                });
                console.log('Vindo da linha 70', JSON.stringify(GDEmGeral[0]));
                return [2 /*return*/];
        }
    });
}); });
// getGD()
var token = ["crAD1zXVz4U:APA91bEehYZeFJ-pwmd1Z6YGzabQVwMBWVeS80cTy3MCwQ02ty58jUA4yr8AJjz5yTGtwm1QQlDZrmYI7kQ05475_mO6MzwaUSEBpusIxuOHHTbANwB2TjyWzyqypHXMNIknqLvFGfVH", "f7oBPYL_LRw:APA91bEATSytBg8XQpb9_Bxb3qX_NWIS7A_yL710afXyBN_q4ejE2X4lwE7fY_LqWL0g5G0qmo-x4AzFCRGsqeKlhJahu4FFrPLlBEkSQKrysa7EohF6WfeQaqTyP7lrHaje5UyzULSO", "eEknOaWkKD0:APA91bGWIYahHkcH9QeKTTQWMIS-b17D8q5AqrYo_VDcuA2Cpu8o5FaIV192EUgpMHe0p39arZ3RxD28BKnGFHZWmbV6LAIuXRgkDUYytg5c-mMOuadmLKzJ1TC4m-uDP6HqHosAdVG6", "e5pw3G9mfc4:APA91bFbHelY-mjw6S9g_2dQw5ryZC9hlA2iqaxBjt-NgNRgxn1E3so4sN8RK9anZmuBOrj8YoeKyKzAdw-ZWCWQc5mH8kxffsoW57fi5FvvlBg_m_axlCznbMOiRZSxHSGf7n0ewrLK", "cq5vUcMOynA:APA91bHujHLzzW9nYVyu6t3Kp9hxbkvoMM6SMDS2i7YLxs5Ck8A94GYlSrzEZ3AOvhqqrlgioJY2FWY2bXbPCGX62eMnw-KJsESwUIg95hWdS6sn5aruwXQkyGaQ382FmbWrKjmJ12j0", "cJveIg-gEJs:APA91bFEmt-N6Uk85FSAREiipWgjqEm0wglTGrcApM31zV6nMXFnp3lKQEeM3wZGU8Q9XCU6DBobRrvWTSVmbubs1Neb9bb-g6sqLSzkl5bewMS8QCKZDCwjRjrCj6pW4ahwpvfoZjaV", "cg-GGX2NUmA:APA91bG8OGM6NEEeosfrweGQJCflxkhP-nA2aemYD-EQK6QGLPVWmP__fiPqLf7bDmnL7OOxGQwG7n5qNe2rweKYyOQfhcKEw9sRdg1IWSTvWrfcQwiESkOwM7v9iMlMUFwM9ZKfi0cG", "d8qod_3glwk:APA91bFw7bz9uty3ucZD18UrRY8qZQkchEnncuYlq_hceqUpVmpV-3zvC4NmU7XQqubzFsxhYg485I8zpR_77jgp31lRfhsEAA1jjzoUhrlNV_PJHEtoaTYDJyr9xFueZbuwni0Af4ko", "fm7dv253E04:APA91bH_J8NjmImkGd6oXKZam3Ytrjo_b30R4LC0q4Jjv9q36YDEdtjLCfA5bRve7ZNk4dIoDu4MTd2M2lDMsobdBYcph0NmLW_u3VfJ5JPDdrrAJZpV33kOLtKkdpwHbSjJJZ0g26jk", "c7q1PbYnF7A:APA91bF-n5Iql_Dhke3_1KliVJeckqECp83jIHfIXl0GEGr6vF1pgq7xkhlUNDUEUuQ4QKMs3eVWb5aFIsSpQlWm5GCQNbdJCAUdO8GBgL5jkBCmhyXCm0lLa3034l1Ndg68-vTqJ2If", "fordk63BAEE:APA91bEDtWSDYoNDXXLo_Cas8vXGp1xMdexTTbGRCTgSMjfTe6dwSaFEAfbN9bCJiMaw_37LPYF8oFr7zibjBgR_3oiXZpehIv5mkp0qhxDnyZJ-54ZnpFAs4Fnr8s2bLSuFbyNI_Crr", "eB_B5h-b2C0:APA91bFbmPsQ0gRcbIvoyaSCYIGGkK4vCAH-vOp5C9VEh3K65x2S7F0h3BtcKuRMMXo-pY4PKDIvDH9kEqrm_fRFmNF-0n92KmoJEw2Fpb-LqLxb4I_JkTLJuKAicavKUI6dJzof0HUN", "fsLisBmzCms:APA91bH-Gi1YZ80g46j_mE42Ah0HFcecknIQKE9T3c1pkseuVPiGXGckL9tJNdaqin_GcJqxQ4igTYpoxqaiFGssl1OEX7wnRBksWuI9ebHsCEsz2kpgJ0hg9PDfBSu4zZ_X8O9kMt3b", "cKikwCG09KA:APA91bH6e1yOg_IJMPGTbSpAN2kIw1M_aLC9o1FgdV_IF3UGd-rUwGcP8CCze7ZlDmKOivkgkYCl2EfgT95OOoiiBSejHR-o6zXhpdCURmkGHZ8r9CZ4CV_x_dPnAjjSvPrPOGhjjEMX", "d174KYoEG6Q:APA91bHflkKCb6TCGjFLDtHlG_Z5AJyTqPL9bUYncSgN4nqkZXHM2GY0WBgYmt28zWvXt6UYLux-Fnb-5DTIELyQ5ZuJdBb34xxlqGEYxz_pkOlPJfbHbyobBXQ53TrWkJNWsVc_FlYb", "fmnBzKCPfJs:APA91bGPU0j315WmMcEx4J19PWe0sS_VcSHn7LMCiT6m93jgnWrBAJ2QONW-JCeF7E5bp6JCNDawqH8BlL7mn6U_LR7ANXZ7xh7G1OfoHAZ91xlw9wMOjQ1L5vHQDxKyLH4FywLycwqA"];
var x = {
    notification: {
        title: 'Teste de Notificação',
        tap: "true",
        body: 'Se você recebeu essa notificação, avisa ao Darci para que ele possa me avisar',
        icon: 'https://firebasestorage.googleapis.com/v0/b/brq-sla.appspot.com/o/iconsForNotification%2FDanger.png?alt=media&token=b1bd2112-5938-4f0f-9c95-b928251a660e',
        color: '#ff0000',
        click_action: "https://brqdash.netlify.com/"
    }
};
var payload = x;
x.notification.title.toLocaleUpperCase();
x.notification.body.toLocaleUpperCase();
admin.messaging().sendToDevice(token, payload)
    .then(function (onPushEnd) { return __awaiter(void 0, void 0, void 0, function () {
    var i, element;
    return __generator(this, function (_a) {
        console.log('Resultado do envio', onPushEnd);
        for (i = 0; i < onPushEnd.results.length; i++) {
            element = onPushEnd.results[i];
            if (element.error) {
                token.splice(i, 1);
                // const tokenAfterPush = 
                console.log('Lista de tokens', JSON.stringify(token));
                // await admin.database().ref('/tokenForPush').set(token);
            }
        }
        return [2 /*return*/];
    });
}); })
    .catch(function (err) {
    console.log('Deu erro', err);
});
/* const concluirPush = ((arrModificado: GDInterface[]) => {
  admin.database().ref('/brq-sla/gerenciamentoDiario').set(arrModificado, ((res) => {
    console.log(res);
  }))
  console.log('Vindo da linha 79', arrModificado[0]);


}) */
