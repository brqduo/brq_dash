import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';


const serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://brq-sla.firebaseio.com"
});

interface GDInterface {
  nome: string;
  dados: {
    metas: {
      prodMensal: number;
      horasPrev: number;
      horasEntregues: number;
      metaCumprida: any;
    }
    saldo: any;
    metaSemanal: {
      semana1: {
        valor: number;
        isSemanaAtual: boolean;
      };
      semana2: {
        valor: number;
        isSemanaAtual: boolean;
      };
      semana3: {
        valor: number;
        isSemanaAtual: boolean;
      };
      semana4: {
        valor: number;
        isSemanaAtual: boolean;
      };
      semana5: {
        valor: number;
        isSemanaAtual: boolean;
      };

    };
  };
};

let GDEmGeral: GDInterface[] = [];
const getGD = (async () => {
  const result = await admin.database().ref('/brq-sla/gerenciamentoDiario').once('value');
  GDEmGeral = result.val()
  //   console.log(GDEmGeral);


  GDEmGeral.forEach((element: GDInterface) => {
    const MetaCumprida = (element.dados.metas.horasEntregues / element.dados.metas.prodMensal) * 100
    element.dados.metas.metaCumprida = Math.round(MetaCumprida)

    let Saldo: any = (element.dados.metas.prodMensal - element.dados.metas.horasEntregues)
    if (Saldo < 0) {
      console.log('saldo é negativo da esteira ' + element.nome, Saldo);
      Saldo = ' ✓ '
      element.dados.saldo = Saldo
    } else {
      element.dados.saldo = Saldo
    }
  });

  console.log('Vindo da linha 70', JSON.stringify(GDEmGeral[0]));

  // concluirPush(GDEmGeral);
})
// getGD()

 let token = ["crAD1zXVz4U:APA91bEehYZeFJ-pwmd1Z6YGzabQVwMBWVeS80cTy3MCwQ02ty58jUA4yr8AJjz5yTGtwm1QQlDZrmYI7kQ05475_mO6MzwaUSEBpusIxuOHHTbANwB2TjyWzyqypHXMNIknqLvFGfVH", "f7oBPYL_LRw:APA91bEATSytBg8XQpb9_Bxb3qX_NWIS7A_yL710afXyBN_q4ejE2X4lwE7fY_LqWL0g5G0qmo-x4AzFCRGsqeKlhJahu4FFrPLlBEkSQKrysa7EohF6WfeQaqTyP7lrHaje5UyzULSO", "eEknOaWkKD0:APA91bGWIYahHkcH9QeKTTQWMIS-b17D8q5AqrYo_VDcuA2Cpu8o5FaIV192EUgpMHe0p39arZ3RxD28BKnGFHZWmbV6LAIuXRgkDUYytg5c-mMOuadmLKzJ1TC4m-uDP6HqHosAdVG6", "e5pw3G9mfc4:APA91bFbHelY-mjw6S9g_2dQw5ryZC9hlA2iqaxBjt-NgNRgxn1E3so4sN8RK9anZmuBOrj8YoeKyKzAdw-ZWCWQc5mH8kxffsoW57fi5FvvlBg_m_axlCznbMOiRZSxHSGf7n0ewrLK", "cq5vUcMOynA:APA91bHujHLzzW9nYVyu6t3Kp9hxbkvoMM6SMDS2i7YLxs5Ck8A94GYlSrzEZ3AOvhqqrlgioJY2FWY2bXbPCGX62eMnw-KJsESwUIg95hWdS6sn5aruwXQkyGaQ382FmbWrKjmJ12j0", "cJveIg-gEJs:APA91bFEmt-N6Uk85FSAREiipWgjqEm0wglTGrcApM31zV6nMXFnp3lKQEeM3wZGU8Q9XCU6DBobRrvWTSVmbubs1Neb9bb-g6sqLSzkl5bewMS8QCKZDCwjRjrCj6pW4ahwpvfoZjaV", "cg-GGX2NUmA:APA91bG8OGM6NEEeosfrweGQJCflxkhP-nA2aemYD-EQK6QGLPVWmP__fiPqLf7bDmnL7OOxGQwG7n5qNe2rweKYyOQfhcKEw9sRdg1IWSTvWrfcQwiESkOwM7v9iMlMUFwM9ZKfi0cG", "d8qod_3glwk:APA91bFw7bz9uty3ucZD18UrRY8qZQkchEnncuYlq_hceqUpVmpV-3zvC4NmU7XQqubzFsxhYg485I8zpR_77jgp31lRfhsEAA1jjzoUhrlNV_PJHEtoaTYDJyr9xFueZbuwni0Af4ko", "fm7dv253E04:APA91bH_J8NjmImkGd6oXKZam3Ytrjo_b30R4LC0q4Jjv9q36YDEdtjLCfA5bRve7ZNk4dIoDu4MTd2M2lDMsobdBYcph0NmLW_u3VfJ5JPDdrrAJZpV33kOLtKkdpwHbSjJJZ0g26jk", "c7q1PbYnF7A:APA91bF-n5Iql_Dhke3_1KliVJeckqECp83jIHfIXl0GEGr6vF1pgq7xkhlUNDUEUuQ4QKMs3eVWb5aFIsSpQlWm5GCQNbdJCAUdO8GBgL5jkBCmhyXCm0lLa3034l1Ndg68-vTqJ2If", "fordk63BAEE:APA91bEDtWSDYoNDXXLo_Cas8vXGp1xMdexTTbGRCTgSMjfTe6dwSaFEAfbN9bCJiMaw_37LPYF8oFr7zibjBgR_3oiXZpehIv5mkp0qhxDnyZJ-54ZnpFAs4Fnr8s2bLSuFbyNI_Crr", "eB_B5h-b2C0:APA91bFbmPsQ0gRcbIvoyaSCYIGGkK4vCAH-vOp5C9VEh3K65x2S7F0h3BtcKuRMMXo-pY4PKDIvDH9kEqrm_fRFmNF-0n92KmoJEw2Fpb-LqLxb4I_JkTLJuKAicavKUI6dJzof0HUN", "fsLisBmzCms:APA91bH-Gi1YZ80g46j_mE42Ah0HFcecknIQKE9T3c1pkseuVPiGXGckL9tJNdaqin_GcJqxQ4igTYpoxqaiFGssl1OEX7wnRBksWuI9ebHsCEsz2kpgJ0hg9PDfBSu4zZ_X8O9kMt3b", "cKikwCG09KA:APA91bH6e1yOg_IJMPGTbSpAN2kIw1M_aLC9o1FgdV_IF3UGd-rUwGcP8CCze7ZlDmKOivkgkYCl2EfgT95OOoiiBSejHR-o6zXhpdCURmkGHZ8r9CZ4CV_x_dPnAjjSvPrPOGhjjEMX", "d174KYoEG6Q:APA91bHflkKCb6TCGjFLDtHlG_Z5AJyTqPL9bUYncSgN4nqkZXHM2GY0WBgYmt28zWvXt6UYLux-Fnb-5DTIELyQ5ZuJdBb34xxlqGEYxz_pkOlPJfbHbyobBXQ53TrWkJNWsVc_FlYb", "fmnBzKCPfJs:APA91bGPU0j315WmMcEx4J19PWe0sS_VcSHn7LMCiT6m93jgnWrBAJ2QONW-JCeF7E5bp6JCNDawqH8BlL7mn6U_LR7ANXZ7xh7G1OfoHAZ91xlw9wMOjQ1L5vHQDxKyLH4FywLycwqA"];

admin.database().ref('/tokenForPush').once('value').then((tokenRes) => {
   token = tokenRes.val()
})

const x = {
  notification: {
    title: 'Teste de Notificação',
    tap: "true",
    body: 'Se você recebeu essa notificação, avisa ao Darci para que ele possa me avisar',
    icon: 'https://firebasestorage.googleapis.com/v0/b/brq-sla.appspot.com/o/iconsForNotification%2FDanger.png?alt=media&token=b1bd2112-5938-4f0f-9c95-b928251a660e',
    color: '#ff0000',
    click_action: "https://brqdash.netlify.com/"
  }
}
const payload: any = x
x.notification.title.toLocaleUpperCase();
x.notification.body.toLocaleUpperCase();
admin.messaging().sendToDevice(token, payload)
  .then(async (onPushEnd) => {
    console.log('Resultado do envio',onPushEnd);
for (let i = 0; i < onPushEnd.results.length; i++) {
  const element =  onPushEnd.results[i];
  if (element.error) {
    token.splice(i, 1)
   // const tokenAfterPush = 
    console.log('Lista de tokens', JSON.stringify(token));
  // await admin.database().ref('/tokenForPush').set(token);
  }
}
  })
  .catch((err) => {
    console.log('Deu erro', err)
  })
/* const concluirPush = ((arrModificado: GDInterface[]) => {
  admin.database().ref('/brq-sla/gerenciamentoDiario').set(arrModificado, ((res) => {
    console.log(res);
  }))
  console.log('Vindo da linha 79', arrModificado[0]);


}) */

